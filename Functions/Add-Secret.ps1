function Add-Secret { 
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [string]$Path,
        [Parameter(Mandatory = $true)]
        [string]$Login,
        [Parameter(Mandatory = $true)]
        [ValidateRange(0,2)]
        [Int]$Type,
        [Parameter(Mandatory = $true,
        HelpMessage="Input password or pattern to generate it, for more use 'get-help new-password'")]
        [String]$Password,
        [Parameter(Mandatory = $true)]
        [string]$URL,
        [Parameter(Mandatory = $true,HelpMessage="Enter one or more tags separated by commas.")]
        [string] $Tags,
        [Parameter(Mandatory = $true)]
        [DateTime]$ExpiresTime) 
    # $secret = New-Secret -TypeOfSecret 1 some 1234 http:// @('tag1', 'tag2') (Get-Date).AddDays(3)
    $f = $MyInvocation.InvocationName
    if($Password[0] -eq '#')
    {
        $Password = New-Password $Password
        Write-Verbose -Message "$f - A new password generated"
    }
    $secret = New-Secret $Login $Type $Password $URL $Tags $ExpiresTime
    $secret | ConvertTo-HashString | Out-File $Path -Append
    Write-Verbose -Message "$f - Secret added to [$path]"
} 



 