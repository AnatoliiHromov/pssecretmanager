function Restore-Password {
    param (
        [Object]$Secret
    )
    $Secrets =  $DbBackup | Where-Object Login -eq $Secret.$Login -and URL -eq $Secret.$URL
    if ($Secret.Length -eq 0) {
        #there needs msg
    }
    elseif ($Secret.Length -eq 1) {
        $Secrets[0].$Password
    }
    else {
        $Secrets.$Password
        $id = Read-Host 'Input Secret number(ID) to restore'
        While ($null -eq (Get-Number $id $Secrets.Length)) {
            $id = Read-Host 'Try again:'
        }
        $Secret.$Password = $Secrets[$id -as [int]].$Password
        Update-Secret $Secret
    }
}