
function Add-Backup {
    param (
        [Object]$Secret
    )    
    $Secrets = $DbBackup | Where-Object Login -eq $Secret.$Login -and URL -eq $Secret.$URL -and Type -eq $Secret.$Type
    If ($Secrets.Length -ge 5) {
        $DbBackup.Remove($Secrets[0])
    }    
    $DbBackup.Add($Secret)
    Save-Database
}