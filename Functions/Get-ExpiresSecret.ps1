function Get-ExpiresSecret {
    param (
        [int]$Days
    )
    $Secrets = @()
    if ($null -ne $Db) {
        $Today = Get-Date
        foreach ($Secret in $Db) {
            if (($Secret.Date - $Today).Days -eq $Days) {
               $Secrets.Add($Secret)
            }
        }
    }
    return $Secrets    
}