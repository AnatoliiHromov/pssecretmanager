#. "C:\Users\AnatoliiGromov\Documents\SecretManager\Functions\SecretTypes.ps1"
function New-Secret {
    param (
        [string] $Login,
        [SecretTypes] $TypeOfSecret,
        [string] $Password,
        [string] $URL,
        [string] $Tags,
        [datetime] $ExpiresTime
        )
<#     $secret = new-object PSObject
    $secret | add-member -type NoteProperty -Name Login -Value $Login
    $secret | add-member -type NoteProperty -Name TypeOfSecret -Value $TypeOfSecret
    $secret | add-member -type NoteProperty -Name Password -Value $Password  
    $secret | add-member -type NoteProperty -Name URL -Value $URL
    $secret | add-member -type NoteProperty -Name Tags -Value $Tags 
    $secret | add-member -type NoteProperty -Name ExpiresTime -Value $ExpiresTime  #>
    $secret = @{
        Login = $Login
        TypeOfSecret = $TypeOfSecret
        Password = $Password
        URL = $URL
        Tags = $Tags.Split(',')
        ExpiresTime =$ExpiresTime
    }
    return $secret 
}