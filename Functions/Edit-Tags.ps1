function Edit-Tags {
    param (
        [string] $Name
    )
    $Secret = Get-Secret $Name
    $Db.Remove($Secret)
    $Secret.Tags
    While (True) {
        $Command = Read-Host 'Input Command'
        switch ($Command) {
            'rm' {
                $id = Read-Host 'Input Tag Id'
                $Secret.Tags = $Secret.Tags | Where-Object { $_ –ne $Secret.Tags[$id] }
            }
            'add' { $Secret.Tags += Read-Host 'Input Tag Id' }
            'exit' { break }        
            Default { }
        }
        break
    }
    $Db.Add($Secret)
    Save-Database
}