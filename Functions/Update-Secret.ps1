function Update-Secret {
    param (
        [object]$Secret
    )
    $Db.Remove($Secret)
    Add-Backup $Secret
    $Secret.Date = $Secret.Date.AddDays($Days)
    $Db.Add($Secret)
    Save-Database
}