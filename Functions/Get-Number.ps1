function Get-Number {
    param (
        [string]$Num,
        [int]$Max
    )
    [int]$number = $Num -as [int]
    if ($null -ne $number -and $number -le $Max) {
        return $number;
    }
    else {
        return $null
    }  
}