function Remove-Secret {
    param (
        [Object]$Secret
    )
    if($Db.Length -gt 0)
    {
        $Db.Remove($Secret)
        Save-Database
    }

}