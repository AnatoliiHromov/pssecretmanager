function Add-ExpireDays {
    param (
        [string]$Name,
        [int]$Days#,
        #[Object[]]$Db
    )
    $Secret = Get-Secret $Name
    If ($null -ne $Secret) {
        $Db.Remove($Secret)
        $Secret.Date = $Secret.Date.AddDays($Days)
        $Db.Add($Secret)
        Save-Database
    }
} 