function Open-Database {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$Path
    ) 
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
    if (-not (Test-Path -Path $Path)) {
        Write-Error -Message "Unable to find file [$path]" -ErrorAction Stop
    }
    else {
        Write-Verbose -Message "$f - Importing data from [$path]"
        [string[]]$data = Get-Content -Path $Path -Encoding UTF8 -ReadCount 0
        $Db = New-Object System.Collections.Generic.List[Object]
        ForEach ($d in $data) {
            try {
                $script = [scriptblock]::Create($d)
                [string[]]$allowedCommands = @("New-Date")
                [string[]]$allowedVariables = @()
                $script.CheckRestrictedLanguage($allowedCommands, $allowedVariables, $false)
                $Db.Add((& $script))
            }
            catch {                
                    Write-Error -Exception $_.Exception -ErrorAction Stop   
            }
            Finally {
                $ErrorActionPreference = $previousErrorAction
            }
        }
        Write-Verbose -Message "$f - Db is opened"
        return $Db
    }
}