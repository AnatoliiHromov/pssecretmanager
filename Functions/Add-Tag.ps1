function Add-Tag {
    param (
        [Object]$Name,
        [string]$Tag
    )
    $Secret = Get-Secret $Name
    $Db.Remove($Secret)
    $Secret.Tags+=$Tag
    $Db.Add($Secret)
    Save-Database
}