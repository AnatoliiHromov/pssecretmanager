function Get-Secrets {
    [CmdletBinding()]
    Param(
        [string] $Name,
        [Parameter(ValueFromPipeline)]
        [System.Collections.Generic.List[Object]]$DataBase,
        [string]$Path,
        [switch]$One
    )
    $f = $MyInvocation.InvocationName
    if ('' -ne $Path) {
        $DataBase = Open-Database -Path $Path
    }
    if ($null -ne $DataBase) {
        if ($Name -ne '') {
            Write-Verbose -Message "$f - Getting secrets by [$Name]"
            $Secrets = $DataBase | Where-Object Login -eq $Name
        }
        else {
            Write-Verbose -Message "$f - Getting all secrets"
            return $DataBase 
        }
    }
    else {
        Write-Error -Message "Cannot open Database" -ErrorAction Stop
    }
    if ($Secrets.Length -eq 0) {
        Write-Error -Message "No secrets with Login $Name" -ErrorAction Stop
        return $null
    }
    elseif ($Secrets.Length -eq 1) {
        return $Secrets[0]
    }
    else {
        if ($One) {
            $i = 0
            foreach ($s in $Secrets) {
                Write-Host "[$i]" ($s | Out-String)
                $i++
            }
       
            Do {
                $id = Read-Host 'Input Secret number(ID)'
                $id = Get-Number $id $Secrets.Length
            }
            While ($null -eq $id)
        
            return $Secrets[$id]
        }
        else { Return $Secrets }
    }
}

    