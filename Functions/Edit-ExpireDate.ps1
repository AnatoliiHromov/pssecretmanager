function Edit-ExpireDate {
    param (
        [string]$Name,
        [DateTime]$Date
    )
    $Secret = Get-Secret $Name
    If ($null -ne $Secret) {
        $Db.Remove($Secret)
        $Secret.Date = $Date
        $Db.Add($Secret)
        Save-Database
    }
}