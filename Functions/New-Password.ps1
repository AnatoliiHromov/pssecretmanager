function New-Password {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [string]$Pattern
    )
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - Reading the Pattern"
    if ($Pattern[0] -match '#') {
        $Count = ''
        for ($i = 1; $i -lt $Pattern.Length; $i++) {
            if ($Pattern[$i] -match "[0-9]") {
                $Count += $Pattern[$i]
            }
            else {
                
                break 
            }
        }
        $Count = $Count -as [int]
        $Password = ''
        :wlabel While ($i -lt $Pattern.Length) {
            switch -CaseSensitive ($Pattern[$i]) {
                "D" {
                    $numbers = 0..9 
                    break
                }
                "A" {
                    $upper = 'a b c d e f g h i j k l m n o p q r s t u v w x y z'
                    $upper = $upper.ToUpper()
                    break
                }
                "a" { $alphabets = 'a b c d e f g h i j k l m n o p q r s t u v w x y z' }
                Default {
                    break wlabel 
                }
            }
            $i++
        }
        if ($Pattern[$i] -eq '%') {
            $i++
            Write-Verbose -Message "$f - Creating password"
            while ($i -lt $Pattern.Length) {
                $number = ''
                switch -CaseSensitive ($Pattern[$i]) {
                    "D" {
                        $i++
                        while ($Pattern[$i] -match "[0-9]") {
                            $number += $Pattern[$i]
                            $i++
                        }
                        $Password += $numbers | Get-Random -Count ($number -as [int])
                        break
                    }
                    "A" {
                        $i++
                        while ($Pattern[$i] -match "[0-9]") {
                            $number += $Pattern[$i]
                            $i++
                        }
                        $Password += $upper.Split(' ') | Get-Random -Count ($number -as [int])
                        break
                    }
                    "a" {
                        $i++
                        while ($Pattern[$i] -match "[0-9]") {
                            $number += $Pattern[$i]
                            $i++
                        }
                        $Password += $alphabets.Split(' ') | Get-Random -Count ($number -as [int]) 
                        break
                    }
                    Default {
                        break 
                    }
                }
                
            }
        }
        $Password = $Password -replace ' ', '' -join ''
        if ($null -eq $Numbers) {
            Write-Error "Follow pattern please to get more use 'get-help new-password'"
        }
        else {
            
            if ($Password.Length -gt $Count) {
                $Password = $Password.Substring(0, $Count - 1)
            }   
            elseif ($Password.Length -lt $Count) {
                $numbers += $upper.Split(' ')
                $numbers += $alphabets.Split(' ')
                $Password += $numbers | Get-Random -Count (($Count -as [int]) - $Password.Length)
            }
            Write-Verbose -Message "$f - Some Password is generated"
            return $Password -replace ' ', '' -join ''
        }
    }
}
