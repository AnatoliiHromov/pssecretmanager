# function Write-Messages

# {

#     [CmdletBinding()]

#     param()   

#     Write-Host "Host message"

#     Write-Output "Output message"

#     Write-Verbose "Verbose message"

#     Write-Warning "Warning message"

#     Write-Error "Error message"

#     Write-Debug "Debug message"

# }

# Write-Messages -Verbose
[string]$Input = 1
$Max = 5
$number = $Input -as [int]
if ($null -ne $number -and $number -le $Max) {
    return $number;
}
else {
    return $null
}  
 