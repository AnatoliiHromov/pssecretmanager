foreach ($file in (Get-ChildItem -file -Path(Join-Path -Path $PSScriptRoot -ChildPath .\functions))) {    
    . ([Scriptblock]::Create([System.IO.File]::ReadAllText($file.FullName, [System.Text.Encoding]::UTF8)))
}

function Get-SecretManagerFunctions {
    <#
.SYNOPSIS
    Internal function
.DESCRIPTION
    Internal function
.NOTES
#>
    Param(
        [switch]$AsString
    )
    $functions = (Get-Command -Module SecretManager | Select-Object -Property Name)
    $functionsString = ($functions.Name) -join "','"
    $functionsString = "'$functionsString'"

    if ($AsString.IsPresent) {
        $functionsString
    }
    else {
        $functions
    }
}